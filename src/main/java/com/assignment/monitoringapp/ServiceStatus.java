package com.assignment.monitoringapp;

public class ServiceStatus {

    private String status;

    public ServiceStatus() {

    }

    public ServiceStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
