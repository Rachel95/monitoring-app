package com.assignment.monitoringapp;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class MonitoringService {

    private static final String SEARCH_SERVICE = "search-service";
    private static final String AD_SERVICE = "ad-service";
    private static final String SEARCH_APP = "search-app";
    private static final String WEB_CRAWLER = "web-crawler";

    @Value("${search.service.url}")
    private String searchServiceUrl;

    @Value("${ad.service.url}")
    private String adServiceUrl;

    @Value("${web.crawler.url}")
    private String webCrawlerUrl;

    @Value("${search.app.url}")
    private String searchAppUrl;

    @Value("${slack.webhook.url}")
    private String slackWebhookUrl;

    private final RestTemplate restTemplate = new RestTemplate();

    public MonitoringService() {
    }

    @Scheduled(initialDelay = 2000, fixedDelay = 5000)
    private void checkSearchApp() {
        checkHealth(searchAppUrl, SEARCH_APP);
    }

    @Scheduled(initialDelay = 2000, fixedDelay = 5000)
    private void checkSearchService() {
        checkHealth(searchServiceUrl, SEARCH_SERVICE);
    }

    @Scheduled(initialDelay = 2000, fixedDelay = 5000)
    private void checkAdService() {
        checkHealth(adServiceUrl, AD_SERVICE);
    }

    @Scheduled(initialDelay = 2000, fixedDelay = 5000)
    private void checkWebcrawler() {
        checkHealth(webCrawlerUrl, WEB_CRAWLER);
    }

    private void checkHealth(final String url,
                             final String name) {
        long startTime = System.currentTimeMillis();
        try {
            ResponseEntity<ServiceStatus> statuResponse = restTemplate.getForEntity(url + "/actuator/health", ServiceStatus.class);
            ServiceStatus status = statuResponse.getBody();
        } catch (Exception e) {
            postToSlack(name + " : " + url + " :  is DOWN");
        }
        long elapsedTime = System.currentTimeMillis() - startTime;

        //if longer than half a second
        if(elapsedTime > 500) {
            postToSlack(name + " : " + url + " :  HIGH LATENCY");
        }
    }

    private void postToSlack(final String message) {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<SlackMessage> request = new HttpEntity<>(new SlackMessage(message), headers);
            restTemplate.postForEntity(slackWebhookUrl, request, Void.class);
        } catch (Exception e) {
            System.out.println("Could not send message to slack");
        }
    }

    private static class SlackMessage {
        private String text;

        public SlackMessage() {
        }

        public SlackMessage(String text) {
            this.text = text;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }
    }
}
